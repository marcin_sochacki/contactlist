﻿namespace ContactList.Droid.Models
{
    public class Contact
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string HomeLocation { get; set; }

        public Contact(string name, string email, string phoneNumber, string homeLocation)
        {
            Name = name;
            Email = email;
            PhoneNumber = phoneNumber;
            HomeLocation = homeLocation;
        }
    }
}
﻿using Android.Widget;

namespace ContactList.Droid.Models
{
    public class ContactViewHolder : Java.Lang.Object
    {
        public TextView NameTextView { get; set; }
        public TextView PhoneNumberTextView { get; set; }
        public TextView HomeLocationTextView { get; set; }
        public ImageView EmailImageView { get; set; }
        public ImageView PhoneImageView { get; set; }
        public ImageView MapImageView { get; set; }
    }
}
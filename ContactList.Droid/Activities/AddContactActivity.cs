﻿
using System;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Provider;
using Android.Widget;
using ContactList.Droid.Helpers;
using ContactList.Droid.Models;
using Java.IO;
using Newtonsoft.Json;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;

namespace ContactList.Droid.Activities
{
    [Activity(Label = "AddContactActivity")]
    public class AddContactActivity : Activity
    {
        private File _file;
        private File _directory;
        private Bitmap _bitmap;
        private ImageView _contactIcon;

        private const int TakePhotoRequestCode = 0;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.AddContact);

            var addButton = FindViewById<Button>(Resource.Id.addContactButton);
            _contactIcon = FindViewById<ImageView>(Resource.Id.contactIconImageView);

            addButton.Click += AddButton_Click;
            _contactIcon.Click += ContactIcon_Click;
        }

        private void ContactIcon_Click(object sender, EventArgs e)
        {
            var intent = new Intent(MediaStore.ActionImageCapture);
            _directory = new File(Environment.GetExternalStoragePublicDirectory(Environment.DirectoryPictures), MainActivity.PhotoFolderName);
            _file = new File(_directory, $"myPhoto_{Guid.NewGuid()}.jpg");

            intent.PutExtra(MediaStore.ExtraOutput, Uri.FromFile(_file));
            StartActivityForResult(intent, TakePhotoRequestCode);
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            var name = FindViewById<EditText>(Resource.Id.nameEditText);
            var email = FindViewById<EditText>(Resource.Id.emailEditText);
            var phoneNumber = FindViewById<EditText>(Resource.Id.phoneNumberEditText);
            var homeLocation = FindViewById<EditText>(Resource.Id.homeLocationEditText);

            var newContact = new Contact(name.Text, email.Text, phoneNumber.Text, homeLocation.Text);

            var intent = new Intent();
            intent.PutExtra(MainActivity.NewContactKey, JsonConvert.SerializeObject(newContact));

            SetResult(Result.Ok, intent);
            Finish();
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (requestCode == TakePhotoRequestCode && data != null)
            {
                var mediaScanIntent = new Intent(Intent.ActionMediaScannerScanFile);
                var contentUri = Uri.FromFile(_file);
                mediaScanIntent.SetData(contentUri);
                SendBroadcast(mediaScanIntent);

                // Display in ImageView. We will resize the bitmap to fit the display.
                // Loading the full sized image will consume to much memory
                // and cause the application to crash.
                var height = _contactIcon.Height;
                var width = _contactIcon.Width;

                using (_bitmap = _file.Path.LoadAndResizeBitmap(width, height))
                {
                    _contactIcon.SetImageBitmap(_bitmap);
                }

                // Dispose of the Java side bitmap.
                GC.Collect(); 
            }
        }
    }
}
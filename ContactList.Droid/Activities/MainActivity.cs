﻿using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Provider;
using Android.Widget;
using ContactList.Droid.Adapters;
using ContactList.Droid.Models;
using Java.IO;
using Newtonsoft.Json;

namespace ContactList.Droid.Activities
{
    [Activity(Label = "ContactList", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity
    {
        private const int AddContactRequestCode = 200;
        public const string NewContactKey = "NEW_CONTACT_KEY";
        public const string PhotoFolderName = "ContactListApp";

        private List<Contact> _contacts;
        private ContactListAdapter _contactListAdapter;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            if (IsThereAnAppToTakePictures())
            {
                CreateDirectoryForPictures();
            }

            // Get our button from the layout resource,
            // and attach an event to it
            var button = FindViewById<Button>(Resource.Id.addButton);

            button.Click += (sender, args) =>
            {
                StartActivityForResult(typeof(AddContactActivity), AddContactRequestCode);
            };

            Initialize();

            var contactListView = FindViewById<ListView>(Resource.Id.contactListView);

            contactListView.Adapter = _contactListAdapter;
            contactListView.ItemLongClick += ContactListView_ItemLongClick;

        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (requestCode != AddContactRequestCode || data == null) return;

            var receivedContact = JsonConvert.DeserializeObject<Contact>(data.GetStringExtra(NewContactKey));

            _contacts.Add(receivedContact);
            _contactListAdapter.NotifyDataSetChanged();
        }

        private void ContactListView_ItemLongClick(object sender, AdapterView.ItemLongClickEventArgs args)
        {
            var currentContact = _contacts[args.Position];

            ShowConfirmDeleteAlert(currentContact);
        }

        #region private methods

        private void ShowConfirmDeleteAlert(Contact currentContact)
        {
            var alert = new AlertDialog.Builder(this).Create();
            alert.SetTitle("Delete Contact");
            alert.SetMessage($"Are you sure you want to delete {currentContact.Name}?");
            alert.SetButton2("Yes", (s, e) =>
            {
                _contacts.Remove(currentContact);
                _contactListAdapter.NotifyDataSetChanged();
            });
            alert.SetButton("No", (s, e) => { });

            alert.Show();
        }

        private void Initialize()
        {
            _contacts = new List<Contact>();

            for (var i = 1; i <= 20; i++)
            {
                _contacts.Add(new Contact($"My Contact {i}", "contact_email@gmail.com", "+48 123 456 789", "52.231701, 21.005980"));
            }

            _contactListAdapter = new ContactListAdapter(_contacts, this);
        }

        private void CreateDirectoryForPictures()
        {
            var directory = new File(Environment.GetExternalStoragePublicDirectory(Environment.DirectoryPictures), PhotoFolderName);
            if (!directory.Exists())
            {
                directory.Mkdirs();
            }
        }

        private bool IsThereAnAppToTakePictures()
        {
            var intent = new Intent(MediaStore.ActionImageCapture);

            IList<ResolveInfo> availableActivities = PackageManager.QueryIntentActivities(intent, PackageInfoFlags.MatchDefaultOnly);

            return availableActivities != null && availableActivities.Count > 0;
        }

        #endregion

    }
}


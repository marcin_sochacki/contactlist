﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Views;
using Android.Widget;
using ContactList.Droid.Models;
using Uri = Android.Net.Uri;

namespace ContactList.Droid.Adapters
{
    public class ContactListAdapter : BaseAdapter<Contact>
    {
        private readonly List<Contact> _contacts;
        private readonly Activity _parent;

        public ContactListAdapter(List<Contact> contacts, Activity parent)
        {
            _contacts = contacts;
            _parent = parent;
        }

        public override Contact this[int position] => _contacts[position];

        public override int Count => _contacts.Count;

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            ContactViewHolder viewHolder = null;

            if (convertView == null)
            {
                convertView = _parent.LayoutInflater.Inflate(Resource.Layout.view_contact, null);

                viewHolder = new ContactViewHolder
                {
                    NameTextView = convertView.FindViewById<TextView>(Resource.Id.nameTextView),
                    PhoneNumberTextView = convertView.FindViewById<TextView>(Resource.Id.phoneNumberTextView),
                    HomeLocationTextView = convertView.FindViewById<TextView>(Resource.Id.homeLocationEditText),
                    EmailImageView = convertView.FindViewById<ImageView>(Resource.Id.emailImageView),
                    PhoneImageView = convertView.FindViewById<ImageView>(Resource.Id.phoneImageView),
                    MapImageView = convertView.FindViewById<ImageView>(Resource.Id.mapImageView)
                };
                viewHolder.EmailImageView.Click += EmailImageView_Click;
                viewHolder.PhoneImageView.Click += PhoneImageView_Click;
                viewHolder.MapImageView.Click += MapImageView_Click;

                convertView.Tag = viewHolder;
            }

            if (viewHolder == null)
            {
                viewHolder = convertView.Tag as ContactViewHolder;
            }

            var contact = _contacts[position];

            viewHolder.NameTextView.Text = contact.Name;
            viewHolder.PhoneNumberTextView.Text = contact.PhoneNumber;

            viewHolder.EmailImageView.Tag = position;
            viewHolder.PhoneImageView.Tag = position;
            viewHolder.MapImageView.Tag = position;

            return convertView;
        }

        private void MapImageView_Click(object sender, EventArgs e)
        {
            if (!(sender is ImageView control)) return;

            var contact = _contacts[(int)control.Tag];
            
            var geoUri = Uri.Parse ($"geo:0,0?q={contact.HomeLocation}({contact.Name})");
         
            var showOnMapIntent = new Intent (Intent.ActionView, geoUri);

            _parent.StartActivity(showOnMapIntent);
        }

        private void PhoneImageView_Click(object sender, EventArgs e)
        {
            if (!(sender is ImageView control)) return;

            var contact = _contacts[(int)control.Tag];

            var dialContactIntent = new Intent(Intent.ActionDial);
            dialContactIntent.SetData(Uri.Parse($"tel:{contact.PhoneNumber}"));

            _parent.StartActivity(dialContactIntent);
        }

        private void EmailImageView_Click(object sender, EventArgs e)
        {
            if (!(sender is ImageView control)) return;

            var contact = _contacts[(int)control.Tag];

            var sendEmailIntent = new Intent(Intent.ActionSend);
            sendEmailIntent.SetType("plain/text");
            sendEmailIntent.PutExtra(Intent.ExtraEmail, new[] { contact.Email });

            _parent.StartActivity(sendEmailIntent);
        }
    }
}